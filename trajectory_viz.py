from bag_lib.bag_provider import BagProvider
import numpy as np
import matplotlib.pyplot as plt
import csv
from collections import defaultdict

map_csv_path = "/home/khalidowlwalid/eufs-master/data/pack_46/rtk_map_2023_04_11-04_28_18_utm.csv"
bag = "/home/khalidowlwalid/eufs-master/data/pack_46/rosbag2_2023_04_09-17_41_10/rosbag2_2023_04_09-17_41_10_0.mcap"
sim_bag = "/home/khalidowlwalid/eufs-master/data/pack_46/oval_george_heriot_sim/rosbag2_2023_04_11-06_59_53_0.db3"

# TODO(Khalid): Fix sim's trajectory, ensure that it is oriented properly
# TODO(Khalid): Top priority, visualize two plots running at the same time!
# TODO(Khalid): Use pygame to show the results?
# TODO(Khalid): Clean up code
# TODO(Khalid): Color cones properly
# TODO(Khalid): Create class for color
# TODO(Khalid): Create function for extracting data
# TODO(Khalid): Interpolate the reference trajectory from pose, to allow for RMSE calculation
# TODO(Khalid): use typing for function

# TODO(Khalid): Change to cones, make it a tuple of (string, hex code)
class Color:

    def __init__(self):
        self.yellow = "#ffff00"
        self.blue = "0000ff"
        self.green = "00ff00"
        self.red = "ff0000"

def sort_cones(map_csv_file):
    """
    Sort cones function is only used for csv maps

    returns dictionary of cones
    """

    # Initialize an empty dictionary with list formatting
    cones_dict = defaultdict(list)

    with open(map_csv_path, newline='') as csvfile:

        map_reader = csv.reader(csvfile)

        # Skip the heading
        next(map_reader)

        for row in map_reader:

            # Formatting
            tag, x, y = row[0], float(row[1]), float(row[2])
            cones_dict[tag].append((x,y))

    cones_dict = {k: np.array(v) for k, v in cones_dict.items()}
    
    return cones_dict

def get_center_point(pose):
    x_coords, y_coords = pose[:,0], pose[:,1]
    return (sum(x_coords)/len(x_coords), sum(y_coords)/len(y_coords))

def get_trajectory(bag, topic, end_row=None, x_offset=0, y_offset=0):
    x_pos, y_pos = [], []
    bag = BagProvider(bag)
    datasets = bag.get_messages(topic)

    # TODO(Khalid): Check if bag actually contains msg
    if end_row is not None:
        pose_msg = datasets['msg'][:end_row]
    else:
        pose_msg = datasets['msg']

    for i, data in enumerate(pose_msg):
        x_pos.append(data.pose.pose.position.x + x_offset)
        y_pos.append(data.pose.pose.position.y + y_offset)

    return np.array(list(zip(x_pos, y_pos)))

def get_map(bag, topic):
    cones_dict = defaultdict(list)

    bag = BagProvider(bag)
    cones_datasets = bag.get_messages(topic)
    cones_msg = cones_datasets['msg'][0]

    cones_dict['blue'] = np.array([(cone.point.x, cone.point.y) for cone in cones_msg.blue_cones])
    cones_dict['yellow'] = np.array([(cone.point.x, cone.point.y) for cone in cones_msg.yellow_cones])
    return cones_dict

def translate(coordinates, translation_coords):
    return np.add(coordinates, translation_coords)

def rotate(theta, coordinate, rotation_origin=np.array([0,0])):

    R = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]])

    coordinate = np.subtract(coordinate,rotation_origin)
    transformed_map = np.dot(R,coordinate.T).T
    transformed_map = np.add(transformed_map,rotation_origin)

    return transformed_map

def transform(coordinates, translation, angle, rotation_origin):
    return np.add(rotate(angle,coordinates,rotation_origin), translation)
    
color = Color()

# Parse the map into dictionary
cones_dict = get_map(bag, "/map")
cones_centre_point = get_center_point(cones_dict['blue'])

# For simulation
cones_sim_dict = sort_cones(map_csv_path)
cones_sim_centre_point = get_center_point(cones_sim_dict['blue'])
cones_blue_cp = get_center_point(cones_sim_dict['blue'])
cones_yellow_cp = get_center_point(cones_sim_dict['yellow'])
average_cp = np.divide(np.add(cones_blue_cp, cones_yellow_cp), 2)

# Rotate simulation map
angle_degree = -6.6
angle_rad = angle_degree*np.pi/180
translation = np.array([-1.6987,0])
cones_sim_dict = {color: rotate(angle_rad, coordinates, rotation_origin=average_cp) for color, coordinates in cones_sim_dict.items()}
cones_sim_dict = {color: np.add(coordinates, translation) for color, coordinates in cones_sim_dict.items()}

cones_sim_new_centre_point = get_center_point(cones_sim_dict['yellow'])
cones_new_centre_point = get_center_point(cones_dict['yellow'])

## TODO(Khalid): Better error message lol
# Skip the header
actual_pose = get_trajectory(bag, "/pose", end_row=1200)
sim_pose = get_trajectory(sim_bag, "/ground_truth/odom")
sim_traj_centre_point = get_center_point(sim_pose)
sim_pose = transform(sim_pose,translation,angle_rad,sim_traj_centre_point)
coarse_sim_pose = sim_pose[::int(len(sim_pose)/200)]
fig, ax = plt.subplots()

# ----------------------TEST----------------------------------#
cones_dict = {k: np.subtract(v, actual_pose[0]) for k,v in cones_dict.items()}
actual_pose = np.subtract(actual_pose, actual_pose[0])

# Plot cones
ax.scatter(*zip(*cones_dict['blue']), c='blue', edgecolors='black')
ax.scatter(*zip(*cones_dict['yellow']), c='yellow', edgecolors='black')

# Plot sim cones
#ax.scatter(*zip(*cones_sim_dict['blue']), c='blue')
#ax.scatter(*zip(*cones_sim_dict['yellow']), c='yellow') 
#ax.scatter(*zip(*cones_sim_dict['car_start']), c='green') 

# Plot trajectory
ax.set_title("Trajectory comparison")
ax.plot(actual_pose[:,0], actual_pose[:,1], label="Actual trajectory")
ax.scatter(coarse_sim_pose[:,0], coarse_sim_pose[:,1], c='red', label="Simulation trajetory")
ax.legend(loc="upper right")
ax.set_xlabel("x(m)")
ax.set_ylabel("y(m)")
plt.show()