import csv
from collections import defaultdict
import numpy as np
import matplotlib.pyplot as plt

map_csv_path = "/home/khalidowlwalid/eufs-master/src/simulation/eufs_sim/eufs_tracks/csv/oval_george_heriot.csv"

def sort_cones(map_csv_file):
    """
    Sort cones function is only used for csv maps

    returns dictionary of cones
    """

    # Initialize an empty dictionary with list formatting
    cones_dict = defaultdict(list)

    with open(map_csv_file, newline='') as csvfile:

        map_reader = csv.reader(csvfile)

        # Skip the heading
        next(map_reader)

        for row in map_reader:

            # Formatting
            tag, x, y = row[0], float(row[1]), float(row[2])
            cones_dict[tag].append((x,y))

    cones_dict = {k: np.array(v) for k, v in cones_dict.items()}

    return cones_dict

def get_center_point(pose):
    x_coords, y_coords = pose[:,0], pose[:,1]
    return (sum(x_coords)/len(x_coords), sum(y_coords)/len(y_coords))

def rotate(theta, coordinate, rotation_origin=np.array([0,0])):

    R = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]])

    coordinate = np.subtract(coordinate,rotation_origin)
    transformed_map = np.dot(R,coordinate.T).T
    transformed_map = np.add(transformed_map,rotation_origin)

    return transformed_map

cones_dict = sort_cones(map_csv_path)
# Determine the origin point of the map
cones_blue_cp = get_center_point(cones_dict['blue'])
cones_yellow_cp = get_center_point(cones_dict['yellow'])
average_cp = np.divide(np.add(cones_blue_cp, cones_yellow_cp), 2)

angle_degree = -10
angle_rad = angle_degree*np.pi/180
transformed_cones_dict = {color: rotate(angle_rad, coordinates, rotation_origin=average_cp) for color, coordinates in cones_dict.items()}

fig, ax = plt.subplots()
#ax.scatter(*zip(*cones_dict['blue']), c='blue', edgecolors='black')
#ax.scatter(*zip(*cones_dict['yellow']), c='yellow', edgecolors='black')
ax.scatter(*zip(*transformed_cones_dict['blue']), c='blue', edgecolors='black')
ax.scatter(*zip(*transformed_cones_dict['yellow']), c='yellow', edgecolors='black')
ax.scatter(cones_blue_cp[0], cones_blue_cp[1], c='black')
ax.scatter(cones_yellow_cp[0], cones_yellow_cp[1], c='red')
ax.scatter(average_cp[0], average_cp[1], c='green')
plt.show()